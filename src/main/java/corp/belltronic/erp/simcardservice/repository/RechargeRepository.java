package corp.belltronic.erp.simcardservice.repository;

import corp.belltronic.erp.simcardservice.model.internal.Recharge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface RechargeRepository extends JpaRepository<Recharge, Long> {

    @Query("select sum(r.value) from Recharge r where r.simcard.employeeName = :employeeName " +
            "                                   and FUNCTION('YEAR', r.recharge_date) = FUNCTION('YEAR', :date)" +
            "                                   and FUNCTION('MONTH', r.recharge_date) = FUNCTION('MONTH', :date)")
    Double getRechargeTotalByMonth(@Param("employeeName") final String employeeName,
                                   @Param("date") final Date date);

    @Query("select sum(r.value) from Recharge r where r.simcard.employeeName = :employeeName ")
    Double getRechargeTotalByEmployee(@Param("employeeName") final String employeeName);

    @Query("select sum(r.value) from Recharge r")
    Double getRechargeTotal();

}
