package corp.belltronic.erp.simcardservice.repository;

import corp.belltronic.erp.simcardservice.model.internal.Assignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface AssignationRepository extends JpaRepository<Assignment, Assignment.AssignmentKey> {

    @Query("select a from Assignment a where a.assignmentKey.employee_name = :employee_name and a.assignmentKey.delivery_date >= :initial_date and a.assignmentKey.delivery_date <= :final_date")
    List<Assignment> assignedReportByDate(@Param("employee_name") final String employee_name,
                                          @Param("initial_date") final Date initial_date,
                                          @Param("final_date") final Date final_date);

    @Query("select a from Assignment a " +
            "where a.assignmentKey.employee_name = :employee_name " +
            "and a.assignmentKey.sub_employee_name = :sub_employee_name " +
            "and a.assignmentKey.delivery_date >= :initial_date " +
            "and a.assignmentKey.delivery_date <= :final_date")
    List<Assignment> subAssignedReportByDate(@Param("employee_name") final String employee_name,
                                             @Param("sub_employee_name") final String sub_employee_name,
                                             @Param("initial_date") final Date initial_date,
                                             @Param("final_date") final Date final_date);
}
