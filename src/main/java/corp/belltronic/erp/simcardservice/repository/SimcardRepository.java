package corp.belltronic.erp.simcardservice.repository;

import corp.belltronic.erp.simcardservice.model.internal.Simcard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface SimcardRepository extends JpaRepository<Simcard, Long> {

    @Query("SELECT s " +
            "FROM Simcard s " +
            "WHERE s.employeeName = :employeeName " +
            "AND s.delivery_date >= :initial_date " +
            "AND s.delivery_date <= :final_date")
    List<Simcard> listAssignedByDate(@Param("employeeName") final String employeeName,
                                     @Param("initial_date") final Date initial_date,
                                     @Param("final_date") final Date final_date);

    @Query("SELECT s " +
            "FROM Simcard s " +
            "WHERE s.employeeName = :employeeName " +
            "AND s.delivery_date >= :initial_date " +
            "AND s.delivery_date <= :final_date " +
            "AND s.type = :type")
    List<Simcard> listAssignedByDateAndType(@Param("employeeName") final String employeeName,
                                            @Param("type") final int type,
                                            @Param("initial_date") final Date initial_date,
                                            @Param("final_date") final Date final_date);
}
