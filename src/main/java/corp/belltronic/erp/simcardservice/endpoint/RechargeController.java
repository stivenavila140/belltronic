package corp.belltronic.erp.simcardservice.endpoint;

import corp.belltronic.erp.simcardservice.service.RechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@CrossOrigin
@RestController
@RequestMapping("/recharge")
public class RechargeController {

    @Autowired
    RechargeService rechargeService;

    @GetMapping(value = "/{employeeName}/{date}")
    public Double getRechargeTotalByMonth(@PathVariable final String employeeName,
                                          @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") final Date date) {
        return rechargeService.getRechargeTotalByMonth(employeeName, date);
    }

}
