package corp.belltronic.erp.simcardservice.endpoint;

import corp.belltronic.erp.simcardservice.model.internal.Assignment;
import corp.belltronic.erp.simcardservice.model.internal.Simcard;
import corp.belltronic.erp.simcardservice.request.AssignmentReportRequest;
import corp.belltronic.erp.simcardservice.service.AssignationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/assignation")
public class AssignationController {

    @Autowired
    AssignationService assignationService;

    @PostMapping
    public List<Simcard> listAssignmentsByDate(@RequestBody AssignmentReportRequest assignmentReportRequest) {
        if (assignmentReportRequest.getSub_employee_name() != null) {
            if (assignmentReportRequest.getType() == 0) {
                return assignationService.listAssignedByDate(assignmentReportRequest.getSub_employee_name(),
                                                             assignmentReportRequest.getInitial_date(),
                                                             assignmentReportRequest.getFinal_date());
            } else {
                return assignationService.listAssignedByDateAndType(assignmentReportRequest.getSub_employee_name(),
                                                                    assignmentReportRequest.getType(),
                                                                    assignmentReportRequest.getInitial_date(),
                                                                    assignmentReportRequest.getFinal_date());
            }
        } else {
            if (assignmentReportRequest.getType() == 0) {
                return assignationService.listSubAssignedByDate(assignmentReportRequest.getEmployee_name(),
                                                                assignmentReportRequest.getInitial_date(),
                                                                assignmentReportRequest.getFinal_date());
            } else {
                return assignationService.listSubAssignedByDateAndType(assignmentReportRequest.getEmployee_name(),
                                                                       assignmentReportRequest.getType(),
                                                                       assignmentReportRequest.getInitial_date(),
                                                                       assignmentReportRequest.getFinal_date());
            }
        }
    }

    @PostMapping(value = "/report")
    public List<Assignment> assignmentsReportByDate(@RequestBody AssignmentReportRequest assignmentReportRequest) {
        if (assignmentReportRequest.getSub_employee_name() == null) {
            return assignationService.assignedReportByDate(assignmentReportRequest.getEmployee_name(),
                                                           assignmentReportRequest.getInitial_date(),
                                                           assignmentReportRequest.getFinal_date());
        } else {
            return assignationService.subAssignedReportByDate(assignmentReportRequest.getEmployee_name(),
                                                              assignmentReportRequest.getSub_employee_name(),
                                                              assignmentReportRequest.getInitial_date(),
                                                              assignmentReportRequest.getFinal_date());
        }
    }
}
