package corp.belltronic.erp.simcardservice.model.internal;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "recargas")
public class Recharge {

    @Id
    private Long id;

    @Column(name = "fecha_recarga")
    private Date recharge_date;

    @Column(name = "valor_recarga")
    private Long value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "telefono")
    private Simcard simcard;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRecharge_date() {
        return recharge_date;
    }

    public void setRecharge_date(Date recharge_date) {
        this.recharge_date = recharge_date;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Simcard getSimcard() {
        return simcard;
    }

    public void setSimcard(Simcard simcard) {
        this.simcard = simcard;
    }
}
