package corp.belltronic.erp.simcardservice.model.internal;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "simcards")
public class Simcard {

    @Column
    private BigInteger ICC;

    @Id
    @Column(name = "numero")
    private Long number;

    @Column(name = "nombreSubdistribuidor")
    private String employeeName;

    @Column(name = "tipo")
    private int type;

    @Column(name = "fecha_entrega")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date delivery_date;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public BigInteger getICC() {
        return ICC;
    }

    public void setICC(BigInteger ICC) {
        this.ICC = ICC;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(Date delivery_date) {
        this.delivery_date = delivery_date;
    }
}
