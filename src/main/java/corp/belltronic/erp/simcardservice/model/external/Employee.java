package corp.belltronic.erp.simcardservice.model.external;

public class Employee {

    private Long identification;
    private String name;
    private Long phone;
    private boolean locatable;

    public Employee() {
        // no-args constructor required by JPA spec
    }

    public Long getIdentification() {
        return identification;
    }

    public void setIdentification(Long identification) {
        this.identification = identification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public boolean isLocatable() {
        return locatable;
    }

    public void setLocatable(boolean locatable) {
        this.locatable = locatable;
    }
}
