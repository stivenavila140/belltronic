package corp.belltronic.erp.simcardservice.model.internal;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "total_assignment")
public class Assignment {

    @EmbeddedId
    AssignmentKey assignmentKey = new AssignmentKey();

    @Column(name = "prepagos")
    private long prepagos;

    @Column(name = "libres")
    private long libres;

    public Assignment() {
    }

    public AssignmentKey getAssignmentKey() {
        return assignmentKey;
    }

    public void setAssignmentKey(AssignmentKey assignmentKey) {
        this.assignmentKey = assignmentKey;
    }

    public long getPrepagos() {
        return prepagos;
    }

    public void setPrepagos(long prepagos) {
        this.prepagos = prepagos;
    }

    public long getLibres() {
        return libres;
    }

    public void setLibres(long libres) {
        this.libres = libres;
    }

    @Embeddable
    public static class AssignmentKey implements Serializable {

        @Column(name = "name")
        private String employee_name;

        @Column(name = "sub_name")
        private String sub_employee_name;

        @Column(name = "delivery_date")
        @JsonFormat(pattern = "yyyy-MM-dd")
        private Date delivery_date;

        public AssignmentKey() {
        }

        public AssignmentKey(String employee_name, Date delivery_date) {
            this.employee_name = employee_name;
            this.delivery_date = delivery_date;
        }

        public Date getDelivery_date() {
            return delivery_date;
        }

        public void setDelivery_date(Date delivery_date) {
            this.delivery_date = delivery_date;
        }

        public String getEmployee_name() {
            return employee_name;
        }

        public void setEmployee_name(String employee_name) {
            this.employee_name = employee_name;
        }

        public String getSub_employee_name() {
            return sub_employee_name;
        }

        public void setSub_employee_name(String sub_employee_name) {
            this.sub_employee_name = sub_employee_name;
        }
    }
}
