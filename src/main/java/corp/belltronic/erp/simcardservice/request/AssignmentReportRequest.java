package corp.belltronic.erp.simcardservice.request;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class AssignmentReportRequest {

    @NotNull
    private String employee_name;
    private String sub_employee_name;
    private int type = 0;
    @NotNull
    private Date initial_date;
    @NotNull
    private Date final_date;

    public AssignmentReportRequest() {
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getSub_employee_name() {
        return sub_employee_name;
    }

    public void setSub_employee_name(String sub_employee_name) {
        this.sub_employee_name = sub_employee_name;
    }

    public Date getInitial_date() {
        return initial_date;
    }

    public void setInitial_date(Date initial_date) {
        this.initial_date = initial_date;
    }

    public Date getFinal_date() {
        return final_date;
    }

    public void setFinal_date(Date final_date) {
        this.final_date = final_date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
