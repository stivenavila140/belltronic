package corp.belltronic.erp.simcardservice.service;

import corp.belltronic.erp.simcardservice.repository.RechargeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class RechargeService {

    @Autowired
    RechargeRepository rechargeRepository;

    public Double getRechargeTotalByMonth(String employeeName, Date date) {
        return rechargeRepository.getRechargeTotalByMonth(employeeName, date);
    }

}
