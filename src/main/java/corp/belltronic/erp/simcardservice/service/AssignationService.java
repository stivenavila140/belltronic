package corp.belltronic.erp.simcardservice.service;

import corp.belltronic.erp.simcardservice.model.internal.Assignment;
import corp.belltronic.erp.simcardservice.model.internal.Simcard;
import corp.belltronic.erp.simcardservice.repository.AssignationRepository;
import corp.belltronic.erp.simcardservice.repository.SimcardRepository;
import corp.belltronic.erp.userservice.model.internal.Subdistributor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class AssignationService {

    private static final Logger LOGGER = Logger.getLogger(AssignationService.class.getName());

    @Autowired
    SimcardRepository simcardRepository;
    @Autowired
    AssignationRepository assignationRepository;

    private RestTemplate restTemplate = new RestTemplate();
    private static final String BASE_URL = "https://belltronic-erp.herokuapp.com";

    public List<Simcard> listSubAssignedByDate(String employee_name, Date initial_date, Date final_date) {
        LOGGER.log(Level.INFO, "Fetching subdistributor simcards of " + employee_name);
        Subdistributor[] subdistributors = restTemplate.getForObject(
                BASE_URL + "/subdistributor/" + employee_name,
                Subdistributor[].class);
        List<Simcard> simcards = new ArrayList<>();
        for (int i = 0; i < subdistributors.length; i++) {
            LOGGER.log(Level.INFO, "Fetching simcards of " + subdistributors[i].getName());
            simcards.addAll(simcardRepository.listAssignedByDate(subdistributors[i].getName(),
                                                                 initial_date,
                                                                 final_date));
        }
        return simcards;
    }

    public List<Simcard> listSubAssignedByDateAndType(String employee_name, int type, Date initial_date, Date
            final_date) {
        Subdistributor[] subdistributors = restTemplate.getForObject(
                BASE_URL + "/subdistributor/" + employee_name,
                Subdistributor[].class);
        List<Simcard> simcards = new ArrayList<>();
        for (int i = 0; i < subdistributors.length; i++) {
            LOGGER.log(Level.INFO, "Fetching simcards of " + subdistributors[i].getName());
            simcards.addAll(simcardRepository.listAssignedByDateAndType(subdistributors[i].getName(),
                                                                        type,
                                                                        initial_date,
                                                                        final_date));
        }
        return simcards;
    }

    public List<Simcard> listAssignedByDate(String employee_name, Date initial_date, Date final_date) {
        return simcardRepository.listAssignedByDate(employee_name, initial_date, final_date);
    }

    public List<Simcard> listAssignedByDateAndType(String employee_name, int type, Date initial_date, Date final_date) {
        return simcardRepository.listAssignedByDateAndType(employee_name, type, initial_date, final_date);
    }

    public List<Assignment> assignedReportByDate(String employee_name, Date initial_date, Date final_date) {
        return assignationRepository.assignedReportByDate(employee_name, initial_date, final_date);
    }

    public List<Assignment> subAssignedReportByDate(String employee_name, String sub_employee, Date initial_date,
                                                    Date final_date) {
        return assignationRepository.subAssignedReportByDate(employee_name, sub_employee, initial_date, final_date);
    }
}
