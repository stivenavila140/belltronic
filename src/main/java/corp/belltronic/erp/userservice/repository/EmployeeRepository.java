package corp.belltronic.erp.userservice.repository;

import corp.belltronic.erp.userservice.model.internal.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query("select e from Employee e where e.type = :type order by e.name")
    List<Employee> listByType(@Param("type") final Employee.Type type);

    @Query("select e from Employee e where e.locatable = true or e.type='SELLER' order by e.name")
    List<Employee> listLocatable();
}
