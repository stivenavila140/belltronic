package corp.belltronic.erp.userservice.repository;

import corp.belltronic.erp.userservice.model.internal.Subdistributor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SubdistributorRepository extends JpaRepository<Subdistributor, String> {

    @Query("select s from Subdistributor s where s.distributor_email = :distributor_email")
    List<Subdistributor> listSubdistributors(@Param("distributor_email") String distributor_email);
}
