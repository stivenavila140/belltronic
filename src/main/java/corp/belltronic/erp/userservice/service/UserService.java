package corp.belltronic.erp.userservice.service;

import corp.belltronic.erp.userservice.model.internal.User;
import corp.belltronic.erp.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    private Logger log = Logger.getLogger(UserService.class.getName());

    public List<User> listUsers() {
        return userRepository.findAll();
    }

    public User saveUser(final User user) {
        return userRepository.save(user);
    }

    public User getUser(final long userIdentification) {
        User user = userRepository.findOne(userIdentification);
        if (user == null) {
            log.log(Level.INFO, () -> "User not found: " + userIdentification);
            user = new User();
        }
        return user;
    }

}
