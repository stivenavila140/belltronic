package corp.belltronic.erp.userservice.service;

import com.google.gson.Gson;
import corp.belltronic.erp.userservice.model.internal.Employee;
import corp.belltronic.erp.userservice.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private static final Logger LOGGER = Logger.getLogger(UserService.class.getName());

    @Autowired
    EmployeeRepository employeeRepository;


    public List<Employee> listEmployees() {
        LOGGER.log(Level.INFO, () -> "Returning all employees");
        return employeeRepository.findAll();
    }

    public List<Employee> listEmployeesByType(Employee.Type type) {
        LOGGER.log(Level.INFO, () -> "Returning all employees of type " + type);
        return employeeRepository.listByType(type);
    }

    public List<Employee> listLocatableEmployees() {
        LOGGER.log(Level.INFO, () -> "Returning all locatable employees");
        return employeeRepository.listLocatable();
    }

    public Employee saveEmployee(final Employee employee) {
        return employeeRepository.save(employee);
    }

    public List<Employee> saveEmployees(final List<Employee> employees) {
        LOGGER.log(Level.INFO, () -> "Saving multiple employees: " + new Gson().toJson(employees));
        return employees.stream()
                .map(employeeRepository::save)
                .collect(Collectors.toList());
    }

    public void deleteEmployee(final long identification) {
        employeeRepository.delete(identification);
    }

    public Employee getEmployee(final long identification) {
        Employee employee = employeeRepository.findOne(identification);
        if (employee == null) {
            LOGGER.log(Level.INFO, () -> "Employee not found: " + identification);
            employee = new Employee();
        }
        return employee;
    }

}
