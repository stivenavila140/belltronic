package corp.belltronic.erp.userservice.service;

import corp.belltronic.erp.userservice.model.internal.User;
import corp.belltronic.erp.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    UserRepository userRepository;

    public User authenticate(User user) {
        User authenticatedUser = userRepository.authenticate(user.getEmail(), user.getPassword());
        return authenticatedUser == null ? new User() : authenticatedUser;
    }

}
