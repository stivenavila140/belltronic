package corp.belltronic.erp.userservice.service;

import corp.belltronic.erp.userservice.model.internal.Subdistributor;
import corp.belltronic.erp.userservice.repository.SubdistributorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class SubdistributorService {

    private static final Logger LOGGER = Logger.getLogger(SubdistributorService.class.getName());

    @Autowired
    SubdistributorRepository subdistributorRepository;

    public List<Subdistributor> listSubEmployeesByName(String employee_name) {
        LOGGER.log(Level.INFO, () -> "Fetching subdistributor of " + employee_name);
        Subdistributor subdistributor = subdistributorRepository.findOne(employee_name);
        if (subdistributor != null) {
            LOGGER.log(Level.INFO, () -> "Returning all sub_employees of " + subdistributor.getEmail());
            return subdistributorRepository.listSubdistributors(subdistributor.getEmail());
        }
        return new ArrayList<>();
    }

}
