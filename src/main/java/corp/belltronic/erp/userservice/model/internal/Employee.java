package corp.belltronic.erp.userservice.model.internal;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @Column
    private Long identification;

    @Column
    @NotNull
    private String name;

    @Column
    private Long phone;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Type type;

    @Column
    private boolean locatable = false;

    public Employee() {
        // no-args constructor required by JPA spec
    }

    public Long getIdentification() {
        return identification;
    }

    public void setIdentification(Long identification) {
        this.identification = identification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isLocatable() {
        return locatable;
    }

    public void setLocatable(boolean locatable) {
        this.locatable = locatable;
    }

    public enum Type {
        SELLER,
        SUBDISTRIBUTOR,
        WHOLESALER
    }
}
