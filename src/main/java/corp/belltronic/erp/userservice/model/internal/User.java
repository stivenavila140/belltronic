package corp.belltronic.erp.userservice.model.internal;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "users")
public class User {

    @Id
    private Long identification;

    @Column(unique = true)
    @NotNull
    private String email;

    @Column
    @NotNull
    private String password;

    @Enumerated(EnumType.STRING)
    @NotNull
    private User.Role role = Role.DEFAULT;

    public User() {
        // no-args constructor required by JPA spec
    }

    public Long getIdentification() {
        return identification;
    }

    public void setIdentification(Long identification) {
        this.identification = identification;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public enum Role {
        ADMINISTRATOR,
        SUBDISTRIBUTOR,
        SELLER,
        ACCOUNTANT,
        DEFAULT
    }
}
