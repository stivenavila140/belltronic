package corp.belltronic.erp.userservice.model.internal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "subdistribuidores")
public class Subdistributor {

    @Id
    @Column(name = "nombre")
    private String name;

    @Column
    @NotNull
    private String email;

    @Column(name = "email_distribuidor")
    private String distributor_email;

    public Subdistributor() {
        // no-args constructor required by JPA spec
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDistributor_email() {
        return distributor_email;
    }

    public void setDistributor_email(String distributor_email) {
        this.distributor_email = distributor_email;
    }
}
