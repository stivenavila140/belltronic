package corp.belltronic.erp.userservice.endpoint;

import corp.belltronic.erp.userservice.model.internal.Subdistributor;
import corp.belltronic.erp.userservice.service.SubdistributorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/subdistributor")
public class SubdistributorController {

    @Autowired
    SubdistributorService subdistributorService;

    @GetMapping(value = "/{employee_name}")
    public List<Subdistributor> listSubEmployees(@PathVariable final String employee_name) {
        return subdistributorService.listSubEmployeesByName(employee_name);
    }
}
