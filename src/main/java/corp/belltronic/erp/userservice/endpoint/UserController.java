package corp.belltronic.erp.userservice.endpoint;

import corp.belltronic.erp.userservice.model.internal.User;
import corp.belltronic.erp.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping
    public List<User> listUsers() {
        return userService.listUsers();
    }

    @PostMapping
    public User saveUser(@RequestBody final User user) {
        return userService.saveUser(user);
    }

    @GetMapping(value = "/{userIdentification}")
    public User getUser(@PathVariable final Long userIdentification) {
        return userService.getUser(userIdentification);
    }
}
