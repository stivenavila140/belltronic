package corp.belltronic.erp.userservice.endpoint;

import corp.belltronic.erp.userservice.model.internal.Employee;
import corp.belltronic.erp.userservice.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping
    public List<Employee> listEmployees() {
        return employeeService.listEmployees();
    }

    @GetMapping(value = "/locatable")
    public List<Employee> listLocatableEmployees() {
        return employeeService.listLocatableEmployees();
    }

    @GetMapping(value = "/type/{type}")
    public List<Employee> listEmployeesByType(@PathVariable final Employee.Type type) {
        return employeeService.listEmployeesByType(type);
    }

    @PostMapping
    public Employee saveEmployee(@RequestBody final Employee user) {
        return employeeService.saveEmployee(user);
    }

    @PostMapping(value = "/multiple")
    public List<Employee> saveEmployees(@RequestBody final List<Employee> employees) {
        return employeeService.saveEmployees(employees);
    }

    @DeleteMapping(value = "/{identification}")
    public void deleteEmployee(@PathVariable final Long identification) {
        employeeService.deleteEmployee(identification);
    }

    @GetMapping(value = "/{identification}")
    public Employee getEmployee(@PathVariable final Long identification) {
        return employeeService.getEmployee(identification);
    }


}
