package corp.belltronic.erp.userservice.endpoint;

import corp.belltronic.erp.userservice.model.internal.User;
import corp.belltronic.erp.userservice.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @PostMapping
    public User authenticate(@RequestBody final User user) {
        return authService.authenticate(user);
    }

}
