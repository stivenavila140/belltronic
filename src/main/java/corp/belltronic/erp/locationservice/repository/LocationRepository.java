package corp.belltronic.erp.locationservice.repository;

import corp.belltronic.erp.locationservice.model.internal.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface LocationRepository extends JpaRepository<Location, Long> {

    @Query("select l from Location l where l.employeeIdentification = :employeeIdentification and l.date = :date order by l.date")
    List<Location> findByUserAndDate(@Param("employeeIdentification") final Long employeeIdentification,
                                     @Param("date") final Date date);

    List<Location> findTop20ByEmployeeIdentificationOrderByDateDesc(
            @Param("employeeIdentification") final Long employeeIdentification);
}
