package corp.belltronic.erp.locationservice.endpoint;

import corp.belltronic.erp.locationservice.model.internal.Location;
import corp.belltronic.erp.locationservice.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/location")
public class LocationController {

    @Autowired
    LocationService locationService;

    @GetMapping
    public List<Location> listLocations() {
        return locationService.listLocations();
    }

    @PostMapping
    public Location saveLocation(@RequestBody final Location location) {
        return locationService.saveLocation(location);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteLocation(@PathVariable final Long id) {
        locationService.deleteLocation(id);
    }

    @GetMapping(value = "/{id}")
    public Location getLocation(@PathVariable final Long id) {
        return locationService.getLocation(id);
    }

    @GetMapping(value = "/user/{userId}")
    public List<Location> getUserLocations(@PathVariable final Long userId) {
        return locationService.getUserLocations(userId);
    }

    @GetMapping(value = "/user/{userId}/{date}")
    public List<Location> getUserLocationsByDate(@PathVariable final Long userId, @PathVariable final Date date) {
        return locationService.getUserLocationsByDate(userId, date);
    }
}
