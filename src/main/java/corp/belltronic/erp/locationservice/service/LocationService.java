package corp.belltronic.erp.locationservice.service;

import corp.belltronic.erp.locationservice.model.external.Employee;
import corp.belltronic.erp.locationservice.model.internal.Location;
import corp.belltronic.erp.locationservice.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class LocationService {

    @Autowired
    LocationRepository locationRepository;

    private Logger log = Logger.getLogger(LocationService.class.getName());
    private RestTemplate restTemplate = new RestTemplate();

    public List<Location> listLocations() {
        return locationRepository.findAll();
    }

    public Location saveLocation(final Location location) {
        log.log(Level.INFO,
                "Save location - save location request for employee: " + location.getEmployeeIdentification());
        Employee employee = restTemplate.getForObject(
                "https://belltronic-erp.herokuapp.com/employee/" + location.getEmployeeIdentification(),
                Employee.class);
        log.log(Level.INFO, "Save location - response from user service: " + employee.getIdentification());
        if (employee.getIdentification() != null) {
            log.log(Level.INFO, "Save location - Location saved for employee: " + employee.getIdentification());
            return locationRepository.save(location);
        } else {
            log.log(Level.INFO, "Save location - employee not found: " + employee.getIdentification());
            return new Location();
        }
    }

    public void deleteLocation(final Long id) {
        log.log(Level.INFO, "delete location - delete location request for id: " + id);
        locationRepository.delete(id);
        log.log(Level.INFO, "delete location - location deleted: " + id);
    }

    public Location getLocation(final long id) {
        Location location = locationRepository.findOne(id);
        if (location == null) {
            log.log(Level.INFO, () -> "Location not found: " + id);
            location = new Location();
        }
        return location;
    }

    public List<Location> getUserLocations(final long userId) {
        return locationRepository.findTop20ByEmployeeIdentificationOrderByDateDesc(userId);
    }

    public List<Location> getUserLocationsByDate(final Long userId, final Date date) {
        return locationRepository.findByUserAndDate(userId, date);
    }
}
